package com.example.stakh.mycalc;

import android.annotation.SuppressLint;
import android.icu.math.BigDecimal;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv;
    double num1, num2;
    int flagAction;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        int[] ids = {R.id.btOne, R.id.btTwo, R.id.btThree, R.id.btFour, R.id.btFive,
                R.id.btSix, R.id.btSeven, R.id.btEight, R.id.btNine, R.id.btZero,
                R.id.btPlus, R.id.btMinus, R.id.btMulti, R.id.btDiv, R.id.btEqual,
                R.id.btClear, R.id.btPoint};

        for (int i : ids) {
            ((Button) findViewById(i)).setOnClickListener(this);
            ((Button) findViewById(i)).setBackgroundColor(0);
            ((Button) findViewById(i)).setTextSize(22);

        }
        tv = findViewById(R.id.tvLCD);

        clearVariables();

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btOne:
                tv.setText(tv.getText() + "1");
                break;
            case R.id.btTwo:
                tv.setText(tv.getText() + "2");
                break;
            case R.id.btThree:
                tv.setText(tv.getText() + "3");
                break;
            case R.id.btFour:
                tv.setText(tv.getText() + "4");
                break;
            case R.id.btFive:
                tv.setText(tv.getText() + "5");
                break;
            case R.id.btSix:
                tv.setText(tv.getText() + "6");
                break;
            case R.id.btSeven:
                tv.setText(tv.getText() + "7");
                break;
            case R.id.btEight:
                tv.setText(tv.getText() + "8");
                break;
            case R.id.btNine:
                tv.setText(tv.getText() + "9");
                break;
            case R.id.btZero:
                tv.setText(tv.getText() + "0");
                break;
            case R.id.btPoint:
                String sequence = tv.getText().toString();
                if (!sequence.contains("."))
                    tv.setText(tv.getText() + ".");
                break;
            case R.id.btPlus:
                flagAction = 1;
                try {
                    num1 = Double.parseDouble(tv.getText().toString());
                    tv.setText("");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btMinus:
                flagAction = 2;
                try {
                    num1 = Double.parseDouble(tv.getText().toString());
                    tv.setText("");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btMulti:
                flagAction = 3;
                try {
                    num1 = Double.parseDouble(tv.getText().toString());
                    tv.setText("");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btDiv:
                flagAction = 4;
                try {
                    num1 = Double.parseDouble(tv.getText().toString());
                    tv.setText("");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btEqual:
                switch (flagAction) {
                    case 1:
                        try {
                            num2 = Double.parseDouble(tv.getText() + "");
                            result = num1 + num2;
                            rounding();
                        } catch (NumberFormatException e) {
                            setToast();
                            clearVariables();
                        }
                        break;
                    case 2:
                        try {
                            num2 = Double.parseDouble(tv.getText() + "");
                            result = num1 - num2;
                            rounding();
                        } catch (NumberFormatException e) {
                            setToast();
                            clearVariables();
                        }
                        break;
                    case 3:
                        try {
                            num2 = Double.parseDouble(tv.getText() + "");
                            result = num1 * num2;
                            rounding();
                        } catch (NumberFormatException e) {
                            setToast();
                            clearVariables();
                        }
                        break;
                    case 4:
                        try {
                            num2 = Double.parseDouble(tv.getText() + "");
                            if (num2 != 0) {
                                result = num1 / num2;
                                rounding();
                            } else
                                Toast.makeText(this, "Нельзя делить на ноль!", Toast.LENGTH_LONG).show();
                        } catch (NumberFormatException e) {
                            setToast();
                            clearVariables();
                        }
                        break;
                    default:
                        Toast.makeText(this, "Операция не задана", Toast.LENGTH_LONG).show();
                }

                if (flagAction != 0) {
                    showNumber(result);
                }
                break;
            case R.id.btClear:
                clearVariables();
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void rounding() {
        if (result > 1 || result < -1)
            result = new BigDecimal(result).setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue();
        else if ( (result < 1 | result > 0) || (result > -1 |  result < 0))
            result = new BigDecimal(result).setScale(11, BigDecimal.ROUND_UP).doubleValue();
    }

    @SuppressLint("SetTextI18n")
    private void showNumber(double numb) {
        if (numb % 1 == 0) {
            tv.setText(Integer.toString((int) numb));
        } else {
            tv.setText((Double.toString(numb)));
        }
    }

    private void clearVariables() {
        num1 = 0;
        num2 = 0;
        result = 0;
        flagAction = 0;
        tv.setText("");
    }
	
	private void setToast() {
		Toast.makeText(this, "Второе число не задано!", Toast.LENGTH_SHORT).show();
	}

}
